const express = require('express');
const fs = require('fs');
const app = express();
const mustacheExpress = require('mustache-express');

const events_contents = fs.readFileSync('events.json');
const events = JSON.parse(events_contents);

app.engine('mustache', mustacheExpress());

app.set('view engine', 'mustache');
app.set('views', __dirname + '/views');
app.use(express.static('public'));

app.get('/', function(req, res) {
  const data = {
    hello: 'WORLD variable',
    foo: 'bar',
    events: events.events
  };

  res.render('index', data);
});

app.listen(3000, () => {
  console.log('Server running at http://localhost:3000/');
});
