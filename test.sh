#!/bin/sh

echo_exit() {
  echo "$*"
  exit 1
}

# Build image
docker image build -t registry.gitlab.com/nodexchange/sophia.events . || echo_exit "Could not use docker"

# Run website
ID="$(docker run -d -p 80 registry.gitlab.com/nodexchange/sophia.events)"

# Get port
PORT="$(docker inspect --format '{{ (index (index .NetworkSettings.Ports "80/tcp") 0).HostPort }}' "$ID")"
echo "=> web site available on http://localhost:$PORT"
