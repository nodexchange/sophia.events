const fs = require("fs"),
      lo = require("lodash"),
      util = require('util');

// Get whole list of events
var events_contents = fs.readFileSync("events.json");
var events = JSON.parse(events_contents);

// Get whole list of sponsors
var sponsors = lo.flatten(lo.map(events.events, function(event) { 
  return lo.map(lo.flatten(event.sponsors), function(sponsor){ 
    return sponsor.name; }) }));

// Make sure image exists for each sponsor
sponsors.forEach(function(sponsor){
  var file = util.format("img/%s.png", sponsor);
  if(!fs.existsSync(file)){
    console.log(util.format("file %s does not exists", file));
    process.exit(1);
  }
});
